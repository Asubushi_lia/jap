* В окне отображения результата тренировки, если все ответы "Знаю" или "Не знаю", то отображаются результаты на все свободное пространство экрана, а не на половину, как было ранее
* Вместо слитых слов в одну цепочку слов, перечисленных через запятую, теперь слова на отдельных строках
* Запрещен поворот окна, при котором менялись слова в тренировке. Теперь только портретный режим
* Изменен размер шрифта русского языка во время тренировки карт памяти
* Исправлена ошибка со словарем при повторном входе (когда в дополнении к словам предыдущего пользователя добавлялись слова нового
* Ускорена работа с сервером (создается только одно FTP соединение для последовательности обращений)