package jap.main

import java.io.FileOutputStream
import java.io.IOException
import java.io.*
import android.content.Context

// Объект для работы с данными аудио и csv-формата общего словаря,
//          файлами пользовательского профиля и прогресса
//          директориями и файлами пользователя
object FsClient {
    fun setCommonDatabase(context: Context, data: String): Boolean {
        return try {
            // Получение общей базы данных на устройство
            val userFile = File(context.filesDir, "database.csv")
            if (!userFile.exists()) {
                if (!userFile.createNewFile()) {
                    return false
                }
            }
            // Запись в файл
            val fOut = FileOutputStream(userFile, false)
            fOut.write(data.toByteArray())
            fOut.close()
            true
        } catch (ex: IOException) {
            false
        }
    }

    fun getCommonDatabase(context: Context): String {
        return try {
            val userFile = File(context.filesDir, "database.csv")
            if (!userFile.exists()) {
                return String()
            }
            val fIn = FileInputStream(userFile)
            return String(fIn.readBytes())
        } catch (ex: IOException) {
            String()
        }
    }

    fun setAudioFile(context: Context, fileName: String, data: ByteArray): Boolean {
        val audioDir = File(context.filesDir, "audio")
        val audioJpDir = File(audioDir, "jp")
        val audioFile = File(audioJpDir, fileName)
        return try {
            if (!audioDir.exists()) {
                if (!audioDir.mkdir()) {
                    return false
                }
            }
            if (!audioJpDir.exists()) {
                if (!audioJpDir.mkdir()) {
                    return false
                }
            }
            if (!audioFile.exists()) {
                if (!audioFile.createNewFile()) {
                    return false
                }
            }
            val fOut = FileOutputStream(audioFile, false)
            fOut.write(data)
            fOut.close()
            true
        } catch (ex: IOException) {
            audioFile.delete()
            false
        }
    }

    fun getAudioList(context: Context): Array<out String?>? {
        return try {
            val audioDir = File(context.filesDir, "audio")
            if (!audioDir.exists()) {
                if (!audioDir.mkdir()) {
                    return null
                }
            }
            val audioJpDir = File(audioDir, "jp")
            if (!audioJpDir.exists()) {
                if (!audioJpDir.mkdir()) {
                    return null
                }
            }
            audioJpDir.list()
        } catch (ex: IOException) {
            null
        }
    }

    fun removeAudioFile(context: Context, fileName: String): Boolean {
        return try {
            val audioDir = File(context.filesDir, "audio")
            if (!audioDir.exists()) {
                if (!audioDir.mkdir()) {
                    return false
                }
            }
            val audioJpDir = File(audioDir, "jp")
            if (!audioJpDir.exists()) {
                if (!audioJpDir.mkdir()) {
                    return false
                }
            }
            val audioJpFile = File(audioJpDir, fileName)
            if (!audioJpFile.exists()) {
                if (!audioJpFile.mkdir()) {
                    return false
                }
            }
            audioJpFile.delete()
            true
        } catch (ex: IOException) {
            false
        }
    }

    fun isUpdatedAudioListExist(context: Context): Boolean {
        val indexData = getUpdatedAudioList(context)
        if (indexData.isEmpty()) {
            return false
        }
        return true
    }

    fun setUpdatedAudioList(
        context: Context,
        data: String
    ): Boolean {
        return try {
            val fOut: FileOutputStream

            val updatedAudioListFile = File(context.filesDir, "updated_audio.csv")
            if (!updatedAudioListFile.exists()) {
                if (!updatedAudioListFile.createNewFile()) {
                    false
                }
            }
            fOut = FileOutputStream(updatedAudioListFile, false)
            fOut.write(data.toByteArray())
            fOut.close()
            true
        } catch (ex: IOException) {
            false
        }
    }

    fun getUpdatedAudioList(context: Context): String {
        return String(getUpdatedAudioListFile(context))
    }

    private fun getUpdatedAudioListFile(context: Context): ByteArray {
        return try {
            val updatedAudioListFile = File(context.filesDir, "updated_audio.csv")
            val fIn = FileInputStream(updatedAudioListFile)
            val data = fIn.readBytes()
            fIn.close()
            data
        } catch (ex: IOException) {
            ByteArray(0)
        }
    }

    fun setUserProgress(context: Context, data: String): Boolean {
        return setUserFile(context, "progress.csv", data)
    }

    fun getUserProgress(context: Context): String {
        return String(getUserFile(context, "progress.csv"))
    }

    fun isUserProfileExist(context: Context): Boolean {
        val rv = isUserDirExist(context)
        if (!rv) {
            return false
        }
        val dbData = getUserProgress(context)
        if (dbData.isEmpty()) {
            return false
        }
        return true
    }

//  Проверка наличия пользовательской директории /user/
    fun createProfile(context: Context): Boolean {
        if (isUserDirExist(context)) {
            if (!removeUserDir(context)) {
                return false
            }
        }
        if (!createUserDir(context)) {
            return false
        }
        return true
    }

    private fun isUserDirExist(context: Context): Boolean {
        val usersDir = File(context.filesDir, "user")
        return usersDir.exists()
    }

    private fun createUserDir(context: Context): Boolean {
        val dir = File(context.filesDir, "user")
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                return false
            }
        }
        return true
    }

    private fun getUserDir(context: Context): File {
        return File(context.filesDir, "user")
    }

    private fun removeUserDir(context: Context): Boolean {
        val userDir = getUserDir(context)
        return userDir.deleteRecursively()
    }

    private fun setUserFile(
        context: Context,
        fileName: String,
        data: String
    ): Boolean {
        return try {
            val fOut: FileOutputStream
            val userDir = getUserDir(context)
            val userFile = File(userDir, fileName)
            if (!userFile.exists()) {
                if (!userFile.createNewFile()) {
                    return false
                }
            }
            fOut = FileOutputStream(userFile, false)
            fOut.write(data.toByteArray())
            fOut.close()
            true
        } catch (ex: IOException) {
            false
        }
    }

    private fun getUserFile(context: Context, fileName: String): ByteArray {
        return try {
            val userDir = getUserDir(context)
            val userFile = File(userDir, fileName)
            val fIn = FileInputStream(userFile)
            val data = fIn.readBytes()
            fIn.close()
            data
        } catch (ex: IOException) {
            ByteArray(0)
        }
    }
}
