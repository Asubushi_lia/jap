package jap.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class TrainingResultRecyclerAdapter(
    private val first: ArrayList<String>,
    private val second: ArrayList<String>
) :
    RecyclerView.Adapter<TrainingResultRecyclerAdapter.MyViewHolder>() {
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvFirst = itemView.findViewById<TextView>(R.id.tvFirst)!!
        val tvSecond = itemView.findViewById<TextView>(R.id.tvSecond)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.result_recyclerview_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvFirst.text = first[position]
        holder.tvSecond.text = second[position]
    }

    override fun getItemCount() = first.size
}