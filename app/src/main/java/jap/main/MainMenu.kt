package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class MainMenu : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_main_menu, container, false)
        v.findViewById<Button>(R.id.btnDictionary).setOnClickListener {
            actInterface?.callNextFragment("MenuDictionary")
        }
        v.findViewById<Button>(R.id.btnTraining).setOnClickListener {
            actInterface?.callNextFragment("MenuTraining")
        }
        v.findViewById<Button>(R.id.btnSettings).setOnClickListener {
            actInterface?.callNextFragment("Settings")
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("Start")
        }
        return v
    }
}