package jap.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class SettingsActions : BaseFragment() {
    private lateinit var ct: Context

    private lateinit var btnUpdateAudioAddedWordsUserDictionary: Button
    private lateinit var btnRemoveAudioRemovedWordsUserDictionary: Button
    private lateinit var btnUpdateAudioAllCommonDictionary: Button
    private lateinit var btnReturn: Button

    private var ws = WaitStatus()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_settings_actions, container, false)
        ct = v.context
        val fm = childFragmentManager.beginTransaction()
        fm.add(R.id.status_container, ws).hide(ws).commit()
        btnUpdateAudioAddedWordsUserDictionary =
            v.findViewById(R.id.btnUpdateAudioAddedWordsUserDictionary)
        btnRemoveAudioRemovedWordsUserDictionary =
            v.findViewById(R.id.btnRemoveAudioRemovedWordsUserDictionary)
        btnUpdateAudioAllCommonDictionary =
            v.findViewById(R.id.btnUpdateAudioAllCommonDictionary)
        btnReturn = v.findViewById(R.id.btnReturn)

        btnUpdateAudioAddedWordsUserDictionary.setOnClickListener {
            lockScreen(true)
            showStatusFragment()
            ws.setText(resources.getString(R.string.tvMsgUpdatingAudioAddedWordsUserDictionary))
            Thread { upgradeAudioFiles() }.start()
        }
        btnRemoveAudioRemovedWordsUserDictionary.setOnClickListener {
            lockScreen(true)
            showStatusFragment()
            ws.setText(resources.getString(R.string.tvMsgRemovingAudioDeletedWordsUserDictionary))
            Thread { removeAudioFiles() }.start()
        }
        btnUpdateAudioAllCommonDictionary.setOnClickListener {
            lockScreen(true)
            showStatusFragment()
            ws.setText(resources.getString(R.string.tvMsgUpdatingAudioAllCommonDictionary))
            Thread { updateAudioAll() }.start()
        }

        btnReturn.setOnClickListener {
            actInterface?.callNextFragment("Settings")
        }

        return v
    }

    private fun upgradeAudioFiles() {
        val serverJpList = DatabaseClient.getUserJpList()
        while (!FtpsClient.login() || !AudioClient.upgradeAudioFiles(
                ct,
                serverJpList,
                onPbFromThread,
                setPercentPbFromThread
            )
        ) {
            FtpsClient.out()
            if (isGetReplay(
                    resources.getString(R.string.adMsgErrEthConnect),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutDownload)
                )
            ) {
                continue
            } else {
                break
            }
        }
        FtpsClient.out()
        clearProgressMsgFromThread()
        offPbFromThread()
        lockScreen(false)
    }

    private fun removeAudioFiles() {
        val userJpList = DatabaseClient.getUserJpList()
        while (!AudioClient.removeAudioFiles(
                ct,
                userJpList,
                onPbFromThread,
                setPercentPbFromThread
            )
        ) {
            if (isGetReplay(
                    resources.getString(R.string.tvMsgErrAccessToInternalFlash),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnReturn)
                )
            ) {
                continue
            } else {
                break
            }
        }
        FtpsClient.out()
        clearProgressMsgFromThread()
        offPbFromThread()
        lockScreen(false)
    }

    private fun updateAudioAll() {
        val cfg = SettingsClient(ct)
        if (cfg.getAutoRemoveUnusedAudioByUserDictionaryAfterReturnState()) {
            showOkMsg(
                resources.getString(R.string.adMsgErrCfgDownloadAllBecauseAutoRemoveIsChecked),
            )
            clearProgressMsgFromThread()
            offPbFromThread()
            lockScreen(false)
            return
        }
        val commonJpList = DatabaseClient.getCommonJpList()
        while (!FtpsClient.login() || !AudioClient.upgradeAudioFiles(
                ct,
                commonJpList,
                onPbFromThread,
                setPercentPbFromThread
            )
        ) {
            FtpsClient.out()
            if (isGetReplay(
                    resources.getString(R.string.adMsgErrEthConnect),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutDownload)
                )
            ) {
                continue
            } else {
                break
            }
        }
        FtpsClient.out()
        clearProgressMsgFromThread()
        offPbFromThread()
        lockScreen(false)
    }

    private fun showStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.show(ws).commit()
    }

    private fun hideStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.hide(ws).commit()
    }


    private fun clearProgressMsgFromThread() {
        activity?.runOnUiThread {
            ws.setText(String())
        }
    }

    private val setPercentPbFromThread = { percent: Int ->
        activity?.runOnUiThread {
            showStatusFragment()
            ws.setProgressPercent(percent)
        }
    }

    private val onPbFromThread = {
        activity?.runOnUiThread { showStatusFragment() }
    }

    private fun offPbFromThread() {
        activity?.runOnUiThread {
            hideStatusFragment()
        }
    }

    private fun lockScreen(lock: Boolean) {
        btnUpdateAudioAddedWordsUserDictionary.isClickable = !lock
        btnRemoveAudioRemovedWordsUserDictionary.isClickable = !lock
        btnUpdateAudioAllCommonDictionary.isClickable = !lock
        btnReturn.isClickable = !lock
    }
}
