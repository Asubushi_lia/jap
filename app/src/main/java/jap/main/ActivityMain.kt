package jap.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import kotlin.system.exitProcess

class ActivityMain : AppCompatActivity(), BaseFragment.Interface {
    private lateinit var view: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val fmt = supportFragmentManager.beginTransaction()
            fmt.add(R.id.container, Start()).commit()
        }
        view = findViewById<View>(android.R.id.content).rootView
        hideSystemUI()
    }

    override fun onStart() {
        super.onStart()
        hideSystemUI()
    }

    override fun onRestart() {
        super.onRestart()
        hideSystemUI()
    }

    override fun onBackPressed() {
    }

    override fun callNextFragment(name: String) {
        val fmt = supportFragmentManager.beginTransaction()
        when (name) {
            "Exit" -> exitProcess(0)
            "Start" -> fmt.replace(R.id.container, Start()).commit()
            "MainMenu" -> fmt.replace(R.id.container, MainMenu()).commit()
            "MenuDictionary" -> fmt.replace(R.id.container, MenuDictionary()).commit()
            "MenuTraining" -> fmt.replace(R.id.container, MenuTraining()).commit()
            "Settings" -> fmt.replace(R.id.container, Settings()).commit()
            "CommonDictionary" -> fmt.replace(R.id.container, CommonDictionary()).commit()
            "PersonalDictionary" -> fmt.replace(R.id.container, PersonalDictionary()).commit()

            "MenuMemoryCards" -> fmt.replace(R.id.container, MenuMemoryCards()).commit()
            "SettingsOptions" -> fmt.replace(R.id.container, SettingsOptions()).commit()
            "SettingsActions" -> fmt.replace(R.id.container, SettingsActions()).commit()
        }
    }

    override fun callNextFragment(name: String, params: Bundle) {
        val fmt = supportFragmentManager.beginTransaction()
        when (name) {
            "TrainingResult" -> {
                val fragment = TrainingResult()
                fragment.arguments = params
                fmt.replace(R.id.container, fragment).commit()
            }
            "MemoryCardsTraining" -> {
                val fragment = MemoryCardsTraining()
                fragment.arguments = params
                fmt.replace(R.id.container, fragment).commit()
            }
        }
    }

    override fun hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, view).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }
}
