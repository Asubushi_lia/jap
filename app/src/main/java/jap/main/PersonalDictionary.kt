package jap.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class PersonalDictionary : BaseFragment() {
    private lateinit var ct: Context
    private lateinit var sWordsCategory: Spinner
    private lateinit var sSortBy: Spinner
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnReturn: Button
    private var ws = WaitStatus()
    private var changeIdList = Vector<Int>()
    private var tagsVector = DatabaseClient.getUserDatabaseTags()
    private val sortingTypes = arrayOf(
        "Сначала новые",
        "Сначала старые",
        "От あ до お",
        "От お до あ",
        "JTR ↑",
        "RTJ ↑",
        "KTJ ↑",
        "JTK ↑",
        "KTR ↑",
        "RTK ↑",
        "JTR ↓",
        "RTJ ↓",
        "KTJ ↓",
        "JTK ↓",
        "KTR ↓",
        "RTK ↓"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_personal_dictionary, container, false)
        ct = v.context

        sWordsCategory = v.findViewById(R.id.sWordsCategory)
        sSortBy = v.findViewById(R.id.sSortBy)
        recyclerView = v.findViewById(R.id.recyclerView)
        btnReturn = v.findViewById(R.id.btnReturn)

        val tags = arrayOf("все") + tagsVector.toTypedArray()
        val spNumAa = ArrayAdapter(ct, R.layout.spinner_item, tags)
        spNumAa.setDropDownViewResource(R.layout.spinner_item)
        sWordsCategory.adapter = spNumAa
        sWordsCategory.avoidDropdownFocus()

        val sortAa = ArrayAdapter(ct, R.layout.spinner_item, sortingTypes)
        sortAa.setDropDownViewResource(R.layout.spinner_item)
        sSortBy.adapter = sortAa
        sSortBy.avoidDropdownFocus()

        val llm = LinearLayoutManager(context)
        recyclerView.layoutManager = LinearLayoutManager(ct)
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            llm.orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)

        recyclerView.adapter = UserDictionaryRecyclerAdapter(
            ct,
            if (sWordsCategory.selectedItemPosition == 0) {
                String()
            } else {
                sWordsCategory.selectedItem.toString()
            },
            getSortType()
        )

        sWordsCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    recyclerView.adapter = UserDictionaryRecyclerAdapter(
                        ct,
                        if (sWordsCategory.selectedItemPosition == 0) {
                            String()
                        } else {
                            sWordsCategory.selectedItem.toString()
                        },
                        getSortType()
                    )
                }
            }

        sSortBy.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    recyclerView.adapter = UserDictionaryRecyclerAdapter(
                        ct,
                        if (sWordsCategory.selectedItemPosition == 0) {
                            String()
                        } else {
                            sWordsCategory.selectedItem.toString()
                        },
                        getSortType()
                    )
                }
            }

        btnReturn.setOnClickListener {
            if (changeIdList.isNotEmpty()) {
                sWordsCategory.isClickable = false
                recyclerView.isClickable = false
                btnReturn.isClickable = false
                showStatusFragment()
                Thread { save() }.start()
            } else {
                actInterface?.callNextFragment("MenuDictionary")
            }
        }

        return v
    }

    private fun getSortType(): DatabaseClient.SortType {
        val pos = sSortBy.selectedItemPosition
        return DatabaseClient.SortType.fromInt(pos)
    }

    private fun save() {
        val progress = DatabaseClient.getCurrentProgress()
        printErrMsgFromThread(resources.getString(R.string.tvMsgSavingUserDictionary))
        while (!FsClient.setUserProgress(ct, progress)) {
            if (isGetReplay(
                    resources.getString(R.string.tvMsgErrAccessToInternalFlash),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutSave)
                )
            ) {
                continue
            } else {
                break
            }
        }
        actInterface?.callNextFragment("MenuDictionary")
    }

    private fun checkWordStateChange(id: Int) {
        if (changeIdList.contains(id)) {
            val index = changeIdList.indexOf(id)
            changeIdList.removeAt(index)
        } else {
            changeIdList.addElement(id)
        }
    }

    private val printErrMsgFromThread = { msg: String ->
        activity?.runOnUiThread { ws.setText(msg) }
    }

    private fun showStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.show(ws).commit()
    }
}