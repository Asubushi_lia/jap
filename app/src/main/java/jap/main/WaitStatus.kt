package jap.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView

class WaitStatus : Fragment() {
    private lateinit var tvProgress: TextView
    private lateinit var pbProgress: ProgressBar
    private var tvProgressStartText= String()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_wait_status, container, false)
        tvProgress = v.findViewById(R.id.tvProgress)
        tvProgress.text = tvProgressStartText
        pbProgress = v.findViewById(R.id.pbProgress)
        return v
    }

    fun setText(text: String) {
        if (this::tvProgress.isInitialized) {
            tvProgress.text = text
        } else {
            tvProgressStartText = text
        }
    }

    fun setProgressPercent(percent: Int) {
        if (this::pbProgress.isInitialized) {
            pbProgress.progress = percent
        }
    }
}