package jap.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class CommonDictionary : BaseFragment() {
    private lateinit var ct: Context
    private lateinit var sWordsCategory: Spinner
    private lateinit var sSortBy: Spinner
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnReturn: Button
    private var ws = WaitStatus()
    private var changeIdList = Vector<Int>()
    private val sortingTypes = arrayOf(
        "Сначала новые",
        "Сначала старые",
        "От あ до お",
        "От お до あ"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_common_dictionary, container, false)
        ct = v.context
        val fm = childFragmentManager.beginTransaction()
        fm.add(R.id.status_container, ws).hide(ws).commit()

        sWordsCategory = v.findViewById(R.id.sWordsCategory)
        sSortBy = v.findViewById(R.id.sSortBy)
        recyclerView = v.findViewById(R.id.recyclerView)
        btnReturn = v.findViewById(R.id.btnReturn)

        val tagsVector = DatabaseClient.getCommonDatabaseTags()
        val tags = arrayOf("все") + tagsVector.toTypedArray()
        val spNumAa = ArrayAdapter(ct, R.layout.spinner_item, tags)
        spNumAa.setDropDownViewResource(R.layout.spinner_item)

        sWordsCategory.adapter = spNumAa
        sWordsCategory.avoidDropdownFocus()

        val sortAa = ArrayAdapter(ct, R.layout.spinner_item, sortingTypes)
        sortAa.setDropDownViewResource(R.layout.spinner_item)
        sSortBy.adapter = sortAa
        sSortBy.avoidDropdownFocus()

        val llm = LinearLayoutManager(ct)
        recyclerView.layoutManager = LinearLayoutManager(ct)
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            llm.orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)
        val checkWordStateChange: (id: Int) -> Unit = fun(id: Int) {
            if (changeIdList.contains(id)) {
                val index = changeIdList.indexOf(id)
                changeIdList.removeAt(index)
            } else {
                changeIdList.addElement(id)
            }
        }
        recyclerView.adapter = CommonDictionaryRecyclerAdapter(
            ct,
            if (sWordsCategory.selectedItemPosition == 0) {
                String()
            } else {
                sWordsCategory.selectedItem.toString()
            },
            getSortType(),
            checkWordStateChange,
        )

        sWordsCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    v: View?,
                    position: Int,
                    id: Long
                ) {
                    recyclerView.adapter = CommonDictionaryRecyclerAdapter(
                        ct,
                        if (sWordsCategory.selectedItemPosition == 0) {
                            String()
                        } else {
                            sWordsCategory.selectedItem.toString()
                        },
                        getSortType(),
                        checkWordStateChange
                    )
                }
            }

        sSortBy.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    v: View?,
                    position: Int,
                    id: Long
                ) {
                    recyclerView.adapter = CommonDictionaryRecyclerAdapter(
                        ct,
                        if (sWordsCategory.selectedItemPosition == 0) {
                            String()
                        } else {
                            sWordsCategory.selectedItem.toString()
                        },
                        getSortType(),
                        checkWordStateChange
                    )
                }
            }

        btnReturn.setOnClickListener {
            if (changeIdList.isNotEmpty()) {
                sWordsCategory.isClickable = false
                recyclerView.isClickable = false
                btnReturn.isClickable = false
                showStatusFragment()
                Thread { save() }.start()
            } else {
                actInterface?.callNextFragment("MenuDictionary")
            }
        }

        return v
    }

    private fun showStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.show(ws).commit()
    }

    private fun getSortType(): DatabaseClient.SortType {
        val pos = sSortBy.selectedItemPosition
        return DatabaseClient.SortType.fromInt(pos)
    }

    private fun save() {
        val progress = DatabaseClient.getCurrentProgress()
        printErrMsgFromThread(resources.getString(R.string.tvMsgSavingUserDictionary))
        while (!FsClient.setUserProgress(ct, progress)) {
            if (isGetReplay(
                    resources.getString(R.string.tvMsgErrAccessToInternalFlash),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutSave)
                )
            ) {
                continue
            } else {
                break
            }
        }
        upgradeAudioFiles()
        removeAudioFiles()
        actInterface?.callNextFragment("MenuDictionary")
    }

    private fun upgradeAudioFiles() {
        if (!SettingsClient(ct).getAutoDownloadAudioForUserDictionaryAfterState()) {
            return
        }
        printErrMsgFromThread(resources.getString(R.string.tvMsgUpgradeAudioInDevice))
        val serverJpList = DatabaseClient.getUserJpList()
        while (!FtpsClient.login() || !AudioClient.upgradeAudioFiles(
                ct,
                serverJpList,
                onPbFromThread,
                setPercentPbFromThread
            )
        ) {
            FtpsClient.out()
            if (isGetReplay(
                    resources.getString(R.string.adMsgErrEthConnect),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutDownload)
                )
            ) {
                continue
            } else {
                break
            }
        }
        FtpsClient.out()
    }

    private fun removeAudioFiles() {
        if (!SettingsClient(ct).getAutoRemoveUnusedAudioByUserDictionaryAfterReturnState()) {
            return
        }
        val serverJpList = DatabaseClient.getUserJpList()
        while (!AudioClient.removeAudioFiles(
                ct,
                serverJpList,
                onPbFromThread,
                setPercentPbFromThread
            )
        ) {
            if (isGetReplay(
                    resources.getString(R.string.tvMsgErrAccessToInternalFlash),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnReturn)
                )
            ) {
                continue
            } else {
                break
            }
        }
    }

    private val printErrMsgFromThread = { msg: String ->
        activity?.runOnUiThread { ws.setText(msg) }
    }

    private val setPercentPbFromThread = { percent: Int ->
        activity?.runOnUiThread {
            showStatusFragment()
            ws.setProgressPercent(percent)
        }
    }

    private val onPbFromThread = {
        activity?.runOnUiThread { showStatusFragment() }
    }
}
