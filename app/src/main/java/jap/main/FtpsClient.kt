package jap.main

import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPReply
import org.apache.commons.net.ftp.FTPSClient
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.InetAddress
import java.net.SocketException
import java.util.*

object FtpsClient {
    private const val domain: String = "japcards.ru"
    private const val login: String = "u1670424_jap_db"
    private const val password: String = "Jap2DbPass"
    private const val port: Int = 21
    private val ftps = FTPSClient()

    fun login(): Boolean {
        try {
            ftps.controlEncoding = "UTF-8"
            ftps.connect(domain, port)
            if (!FTPReply.isPositiveCompletion(ftps.replyCode)) {
                return false
            }
            ftps.setDataTimeout(1000)
            ftps.enterLocalPassiveMode()
            if (!ftps.login(login, password)) {
                out()
                return false
            }
        } catch (e: SocketException) {
            return false
        } catch (e: IOException) {
            return false
        } catch (e: java.net.UnknownHostException) {
            return false
        }

        return true
    }

    fun out() {
        try {
            if (ftps.isAvailable) {
                ftps.logout()
                ftps.disconnect()
            }
        } catch (e: SocketException) {
            return
        } catch (e: IOException) {
            return
        } catch (e: java.net.UnknownHostException) {
            return
        }
    }

    fun getCommonDatabase(): String {
        ftps.setFileType(FTP.ASCII_FILE_TYPE);
        return String(readFile("/", "database.csv"))
    }

    fun getAudioFile(jp: String): ByteArray {
        ftps.setFileType(FTP.BINARY_FILE_TYPE);
        return readFile("audio/jp/", jp)
    }

    fun getAudioList(): Vector<String> {
        val list = getFileList("audio/jp/") ?: return Vector()
        val v = Vector<String>()
        for (i in list) {
            val s = i.removePrefix("audio/jp/")
            if (s == "." || s == "..") {
                continue
            }
            v.addElement(s)
        }
        return v
    }

    private fun getFileList(dirPath: String): Array<out String>? {
        return try {
            ftps.listNames(dirPath)
        } catch (e: SocketException) {
            null
        } catch (e: IOException) {
            null
        } catch (e: java.net.UnknownHostException) {
            null
        }
    }

    private fun readFile(pathDir: String, fileName: String): ByteArray {
        return try {
            val passStream = ByteArrayOutputStream()
            if (!ftps.retrieveFile("$pathDir/$fileName", passStream)) {
                return ByteArray(0)
            }
            passStream.toByteArray()
        } catch (e: SocketException) {
            ByteArray(0)
        } catch (e: IOException) {
            ByteArray(0)
        } catch (e: java.net.UnknownHostException) {
            ByteArray(0)
        }
    }
}