package jap.main

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.Integer.min
import java.util.*

class MemoryCardsTraining : BaseFragment() {
    private lateinit var ct: Context
    private lateinit var type: DatabaseClient.Types
    private lateinit var list: Vector<DatabaseClient.UserItem>
    private lateinit var btnWord: Button
    private lateinit var btnKnow: Button
    private lateinit var btnDonTKnow: Button
    private lateinit var btnPlayAgain: Button
    private lateinit var rv: RecyclerView
    private lateinit var pB: ProgressBar

    private var numWord = 0
    private var plus = Vector<Int>()
    private var minus = Vector<Int>()
    private var curIndex = 0

    private var notFirstClick = false
    private var isSecondSide = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_memory_cards_training, container, false)
        ct = v.context

        btnWord = v.findViewById(R.id.btnWord)
        btnKnow = v.findViewById(R.id.btnKnow)
        btnDonTKnow = v.findViewById(R.id.btnDonTKnow)
        btnPlayAgain = v.findViewById(R.id.btnPlayAgain)
        rv = v.findViewById(R.id.rv)
        pB = v.findViewById(R.id.pB)

        type = DatabaseClient.Types.fromInt(arguments?.getInt("type", 0) ?: 0)
        list = DatabaseClient.getSortUserDatabase(
            type, arguments?.getString("tag") ?: String()
        )
        numWord = min(arguments?.getInt("wordNum", 0) ?: 0, list.size)

        for (i in list.size - 1 downTo numWord) {
            list.removeElementAt(i)
        }
        list.shuffle()

        btnWord.setOnClickListener {
            if (!notFirstClick) {
                setEnable(true)
                notFirstClick = true
            }
            isSecondSide = !isSecondSide
            if (isSecondSide) {
                setSecondSide()
            } else {
                setFirstSide()
            }
        }
        btnKnow.setOnClickListener {
            yesClicked()
            yesNoHandler()
        }
        btnDonTKnow.setOnClickListener {
            noClicked()
            yesNoHandler()
        }
        btnPlayAgain.setOnClickListener{
            val context = ct
            AudioClient.playAudio(context, getCurrentJp())
        }
        setFirstSide()
        setEnable(false)

        return v
    }

// Установить первую сторону карточки с вопросом
    private fun setFirstSide() {
        val fSize: Float = when {
            isFirstSideKanji() -> {
                resources.getDimension(R.dimen.btnKanjiSize)
            }
            isFirstSideJp() -> {
                resources.getDimension(R.dimen.btnJpSize)
            }
            else -> {
                resources.getDimension(R.dimen.btnRuSize)
            }
        }

        val sideTextList = getFirstSideText()
        setBtnCfg(sideTextList, fSize)

        val jp = getCurrentJp()
        if (isFirstSideJp() &&
            SettingsClient(ct).getAudioPlayingState() &&
            AudioClient.isAudioExist(jp)
        ) {
            AudioClient.playAudio(ct, jp)
            btnPlayAgain.visibility = View.VISIBLE
        } else {
            btnPlayAgain.visibility = View.INVISIBLE
        }
    }

// Установить вторую сторону карточки с ответом
    private fun setSecondSide() {
        val fSize: Float = when {
            isSecondSideKanji() -> {
                resources.getDimension(R.dimen.btnKanjiSize)
            }
            isSecondSideJp() -> {
                resources.getDimension(R.dimen.btnJpSize)
            }
            else -> {
                resources.getDimension(R.dimen.btnRuSize)
            }
        }

        val sideTextList = getSecondSideText()
        setBtnCfg(sideTextList, fSize)

        val jp = getCurrentJp()
        if (SettingsClient(ct).getAudioPlayingState() &&
            (isSecondSideKanji() || isSecondSideJp()) &&
            AudioClient.isAudioExist(jp)
        ) {
            AudioClient.playAudio(ct, jp)
            btnPlayAgain.visibility = View.VISIBLE
        } else {
            btnPlayAgain.visibility = View.INVISIBLE
        }
    }

// Карточку слова с разделителями (много значений) или без(1 значение у слова)?
    private fun setBtnCfg(sideTextList: Vector<String>, size: Float) {
        if (sideTextList.size == 1) {
            btnWord.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
            rv.layoutManager = null
            btnWord.text = sideTextList[0]
        } else {
            val llm = LinearLayoutManager(ct)
            val dividerItemDecoration = DividerItemDecoration(
                ct,
                llm.orientation
            )
            btnWord.text = ""
            rv.layoutManager = LinearLayoutManager(ct)
            rv.addItemDecoration(dividerItemDecoration)
            rv.adapter = TrainingButtonRecyclerAdapter(sideTextList, size)
        }
    }

    private fun yesNoHandler() {
        notFirstClick = false
        isSecondSide = false
        setEnable(false)
        pB.progress = getCurPercent()
        if (isFinish()) {
            endTraining()
        } else {
            setFirstSide()
        }
    }

    private fun setEnable(state: Boolean) {
        btnKnow.isEnabled = state
        btnDonTKnow.isEnabled = state
    }

    private fun endTraining() {
        setEnable(false)
        btnWord.isEnabled = false
        pB.progress = 100
        setChangeToDatabase()
        Thread { save() }.start()
    }

    private fun save() {
        val progress = DatabaseClient.getCurrentProgress()
        while (!FsClient.setUserProgress(ct, progress)) {
            if (isGetReplay(
                    resources.getString(R.string.tvMsgErrAccessToInternalFlash),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutSave)
                )
            ) {
                continue
            } else {
                break
            }
        }

        val args = Bundle()
        args.putStringArrayList("know_first", getKnowArrayFirst())
        args.putStringArrayList("know_second", getKnowArraySecond())
        args.putStringArrayList("don_t_know_first", getDonTKnowArrayFirst())
        args.putStringArrayList("don_t_know_second", getDonTKnowArraySecond())
        args.putString(
            "return_fragment_name",
            arguments?.getString("return_fragment_name") ?: "TrainingResult"
        )
        actInterface?.callNextFragment("TrainingResult", args)
    }

    private fun getFirstSideText(): Vector<String> {
        return when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.JpToKanji -> {
                val v = Vector<String>()
                v.addElement(DatabaseClient.getCommonDatabaseItem(list[curIndex].id).jp)
                v
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.RuToKanji -> {
                DatabaseClient.getCommonDatabaseItem(list[curIndex].id).ru
            }
            DatabaseClient.Types.KanjiToJp, DatabaseClient.Types.KanjiToRu -> {
                val v = Vector<String>()
                v.addElement(DatabaseClient.getCommonDatabaseItem(list[curIndex].id).kanji)
                v
            }
        }
    }

    private fun getSecondSideText(): Vector<String> {
        return when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.KanjiToRu -> {
                DatabaseClient.getCommonDatabaseItem(list[curIndex].id).ru
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.KanjiToJp -> {
                val v = Vector<String>()
                v.addElement(DatabaseClient.getCommonDatabaseItem(list[curIndex].id).jp)
                v
            }
            DatabaseClient.Types.JpToKanji, DatabaseClient.Types.RuToKanji -> {
                val v = Vector<String>()
                v.addElement(DatabaseClient.getCommonDatabaseItem(list[curIndex].id).kanji)
                v
            }
        }
    }

    private fun isFirstSideKanji(): Boolean {
        return (type == DatabaseClient.Types.KanjiToJp) || (type == DatabaseClient.Types.KanjiToRu)
    }

    private fun isFirstSideJp(): Boolean {
        return (type == DatabaseClient.Types.JpToRu) || (type == DatabaseClient.Types.JpToKanji)
    }

    private fun isSecondSideKanji(): Boolean {
        return (type == DatabaseClient.Types.JpToKanji) || (type == DatabaseClient.Types.RuToKanji)
    }

    private fun isSecondSideJp(): Boolean {
        return (type == DatabaseClient.Types.RuToJp) || (type == DatabaseClient.Types.KanjiToJp)
    }

    private fun getCurrentJp(): String {
        return DatabaseClient.getCommonDatabaseItem(list[curIndex].id).jp
    }

    private fun getCurPercent(): Int {
        val percent = 100.0 / numWord.toFloat() * curIndex
        return percent.toInt()
    }

    private fun yesClicked() {
        plus.addElement(curIndex)
        curIndex++
    }

    private fun noClicked() {
        minus.addElement(curIndex)
        curIndex++
    }

    private fun isFinish(): Boolean {
        return curIndex == list.size
    }

// Обновляем прогресс слов
    private fun setChangeToDatabase() {
        for (i in plus) {
            DatabaseClient.incItem(type, list[i].id)
        }
        for (i in minus) {
            DatabaseClient.decItem(type, list[i].id)
        }
    }

// Получить массив значений
//      для левого столбца узнанных слов
    private fun getKnowArrayFirst(): ArrayList<String> {
        val l = ArrayList<String>()
        when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.JpToKanji -> for (i in plus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).jp)
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.RuToKanji -> for (i in plus) {
                l.add(getRuText(i))
            }
            DatabaseClient.Types.KanjiToJp, DatabaseClient.Types.KanjiToRu -> for (i in plus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).kanji)
            }
        }
        return l
    }

// Получить массив значений
//      для правого столбца узнанных слов
    private fun getKnowArraySecond(): ArrayList<String> {
        val l = ArrayList<String>()
        when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.KanjiToRu -> for (i in plus) {
                l.add(getRuText(i))
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.KanjiToJp -> for (i in plus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).jp)
            }
            DatabaseClient.Types.RuToKanji, DatabaseClient.Types.JpToKanji -> for (i in plus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).kanji)
            }
        }
        return l
    }

// Получить массив значений
//      для левого столбца неузнанных слов
    private fun getDonTKnowArrayFirst(): ArrayList<String> {
        val l = ArrayList<String>()
        when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.JpToKanji -> for (i in minus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).jp)
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.RuToKanji -> for (i in minus) {
                l.add(getRuText(i))
            }
            DatabaseClient.Types.KanjiToJp, DatabaseClient.Types.KanjiToRu -> for (i in minus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).kanji)
            }
        }
        return l
    }

// Получить массив значений
//      для правого столбца неузнанных слов
    private fun getDonTKnowArraySecond(): ArrayList<String> {
        val l = ArrayList<String>()
        when (type) {
            DatabaseClient.Types.JpToRu, DatabaseClient.Types.KanjiToRu -> for (i in minus) {
                l.add(getRuText(i))
            }
            DatabaseClient.Types.RuToJp, DatabaseClient.Types.KanjiToJp -> for (i in minus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).jp)
            }
            DatabaseClient.Types.RuToKanji, DatabaseClient.Types.JpToKanji -> for (i in minus) {
                l.add(DatabaseClient.getCommonDatabaseItem(list[i].id).kanji)
            }
        }
        return l
    }

    private fun getRuText(index: Int): String {
        var commonStr = ""
        var isFirst = true
        for (i in DatabaseClient.getCommonDatabaseItem(list[index].id).ru) {
            if (!isFirst) {
                commonStr += '\n'
            } else {
                isFirst = false
            }
            commonStr += i
        }
        return commonStr
    }
}
