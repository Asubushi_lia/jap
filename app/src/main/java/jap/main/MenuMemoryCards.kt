package jap.main

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

class MenuMemoryCards : BaseFragment() {
    private lateinit var ct: Context
    private lateinit var memoryCardsNumber: Spinner
    private lateinit var memoryCardsTag: Spinner
    private val spNumValues = arrayOf("5", "10", "15", "30", "50", "75", "100", "300")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_menu_memory_cards, container, false)
        ct = v.context

        memoryCardsNumber = v.findViewById(R.id.memoryCardsNumber)
        memoryCardsTag = v.findViewById(R.id.memoryCardsTag)

        val appSs = SettingsClient(ct)

        val spNumAa = ArrayAdapter(ct, R.layout.spinner_item, spNumValues)
        spNumAa.setDropDownViewResource(R.layout.spinner_item)
        memoryCardsNumber.adapter = spNumAa
        memoryCardsNumber.avoidDropdownFocus()
        val tagsVector = DatabaseClient.getUserDatabaseTags()
        val tags = arrayOf("все") + tagsVector.toTypedArray()
        val spTagAa = ArrayAdapter(ct, R.layout.spinner_item, tags)
        spTagAa.setDropDownViewResource(R.layout.spinner_item)
        memoryCardsTag.adapter = spTagAa
        memoryCardsTag.avoidDropdownFocus()
        if (tags.contains(appSs.getMemoryCardsTag())) {
            memoryCardsTag.setSelection(tags.indexOf(appSs.getMemoryCardsTag()))
        }
        memoryCardsTag.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    appSs.setMemoryCardsTag(spTagAa.getItem(position) ?: "все")
                }
            }

        memoryCardsNumber.setSelection(appSs.getMemoryCardsNumber())
        memoryCardsNumber.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    appSs.saveMemoryCardsNumber(position)
                }
            }

        v.findViewById<Button>(R.id.btnJtr).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.JpToRu)
        }
        v.findViewById<Button>(R.id.btnRtj).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.RuToJp)
        }
        v.findViewById<Button>(R.id.btnKtj).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.KanjiToJp)
        }
        v.findViewById<Button>(R.id.btnJtk).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.JpToKanji)
        }
        v.findViewById<Button>(R.id.btnKtr).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.KanjiToRu)
        }
        v.findViewById<Button>(R.id.btnRtk).setOnClickListener {
            callMemoryCardsTraining(DatabaseClient.Types.RuToKanji)
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("MenuTraining")
        }

        return v
    }

    private fun callMemoryCardsTraining(type: DatabaseClient.Types) {
        val tag = getTrTag()
        if (DatabaseClient.getSortUserDatabase(type, tag).isNotEmpty()) {
            val args = Bundle()
            args.putInt("type", type.value)
            args.putInt("wordNum", memoryCardsNumber.selectedItem.toString().toInt())
            args.putString("tag", tag)
            args.putString("return_fragment_name", "MenuMemoryCards")
            actInterface?.callNextFragment("MemoryCardsTraining", args)
        } else {
            emptyList()
        }
    }

    private fun emptyList() {
        val builder = AlertDialog.Builder(ct)
        with(builder)
        {
            setMessage(resources.getString(R.string.adMsgNoWordsToPractice))
            setNeutralButton(resources.getString(R.string.btnReturn), dialogReplay)
            show()
        }
    }

    private val dialogReplay = { _: DialogInterface, _: Int ->
    }

    private fun getTrTag(): String {
        return if (memoryCardsTag.selectedItemPosition == 0) {
            String()
        } else {
            memoryCardsTag.selectedItem.toString()
        }
    }
}