package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox

// Установка опций
class SettingsOptions : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_settings_options, container, false)
        val ct = v.context

        val cfg = SettingsClient(ct)

        val cbPlayAudio = v.findViewById<CheckBox>(R.id.cbPlayAudio)
        cbPlayAudio.isChecked =
            cfg.getAudioPlayingState()
        cbPlayAudio.setOnCheckedChangeListener { _, isChecked ->
            cfg.setAudioPlayingState(isChecked)
        }

//      Состояние опции "обновление текстовой информации словарей при каждом запуске"
        val cbEveryLoginUpgradeFullTextInfoOfDictionaries =
            v.findViewById<CheckBox>(R.id.cbEveryLoginUpgradeFullTextInfoOfDictionaries)
        cbEveryLoginUpgradeFullTextInfoOfDictionaries.isChecked =
            cfg.getEveryLoginUpgradeTextDictionariesState()
        cbEveryLoginUpgradeFullTextInfoOfDictionaries.setOnCheckedChangeListener { _, isChecked ->
            cfg.setEveryLoginUpgradeTextDictionariesState(isChecked)
        }

//      Состояние опции "обновление аудио всех произношений при каждом запуске"
        val cbEveryLoginUpgradeAllAudio =
            v.findViewById<CheckBox>(R.id.cbEveryLoginUpgradeAllAudio)
        cbEveryLoginUpgradeAllAudio.isChecked =
            cfg.getEveryLoginUpgradeAllAudioState()
        cbEveryLoginUpgradeAllAudio.setOnCheckedChangeListener { _, isChecked ->
            cfg.setEveryLoginUpgradeAllAudioState(isChecked)
        }

//      Состояние опции "обновление аудио ЛС при каждом запуске"
        val cbEveryLoginUpgradeAudioForUserDictionary =
            v.findViewById<CheckBox>(R.id.cbEveryLoginUpgradeAudioForUserDictionary)
        cbEveryLoginUpgradeAudioForUserDictionary.isChecked =
            cfg.getEveryLoginUpgradeAudioForUserState()
        cbEveryLoginUpgradeAudioForUserDictionary.setOnCheckedChangeListener { _, isChecked ->
            cfg.setEveryLoginUpgradeAudioForUserState(isChecked)
        }

        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("Settings")
        }

        return v
    }
}
