package jap.main

import android.content.Context
import android.media.MediaPlayer
import java.text.SimpleDateFormat
import java.util.*

object AudioClient {
    private var fsAudioFileList = Vector<String>()

    data class UpdatedAudioList(
        var audiofilename: String = String(),
        var date: String = String(),
    )

    fun initFsAudioList(context: Context): Boolean {
        fsAudioFileList.clear()
        // Получаем список аудио
        val fsAudioList = FsClient.getAudioList(context) ?: return false
        for (i in fsAudioList) {
            if (i == null) {
                continue
            }
            // Добавляем элемент, если не пустой
            fsAudioFileList.addElement(i)
        }
        return true
    }

    // Подгрузка аудио слов с сервера
    fun upgradeAudioFiles(
        context: Context,
        jp: Vector<String>,
        onProgressBar: () -> Unit?,
        updateProgressBar: (Int) -> Unit?
    ): Boolean {
        val ftpAudioList = FtpsClient.getAudioList()
        val downloadFileList = Vector<String>()
//      Индексация аудиофайлов
        for (i in jp) {
            val fileName = "$i.wav"
            if (!fsAudioFileList.contains(fileName)) {
                if (ftpAudioList.contains(fileName)) {
                    downloadFileList.addElement(fileName)
                } else {
                    continue
                }
            }
            var suffix = 1
            do {
                val fileNameWithSuffix = i + "_" + suffix.toString() + ".wav"
                if (!fsAudioFileList.contains(fileNameWithSuffix)) {
                    if (ftpAudioList.contains(fileNameWithSuffix)) {
                        downloadFileList.addElement(fileNameWithSuffix)
                    } else {
                        break
                    }
                }
                suffix++
            } while (true)
        }
        if (downloadFileList.isEmpty()) {
            return true
        }
        val fNum = downloadFileList.size
        var fCounter = 0
        onProgressBar()
        updateProgressBar(0)
//      Загрузить аудио из списка
        val UpdatedAudioListData = FsClient.getUpdatedAudioList(context)
        for (i in downloadFileList) {
            //  Не загружено ли аудио недавно?
            if (!FsClient.isUpdatedAudioListExist(context)) {
                FsClient.setUpdatedAudioList(context, "")
            } else {
                if (isContainsAudio(i)) {
                    if (isUpdateAudioDateMatch(i))
                        continue
                }
            }

            val fileData = FtpsClient.getAudioFile(i)
            if (fileData.isEmpty()) {
                return false
            }
            if (!FsClient.setAudioFile(context, i, fileData)) {
                return false
            }
            fsAudioFileList.addElement(i)
            setUpdatedAudio(i)  // the same way to set & change value
            fCounter++
            updateProgressBar((100.0F / fNum * fCounter).toInt())
        }
        FsClient.setUpdatedAudioList(context, getAuListStr(updatedAudioList))
        return true
    }

    fun removeAudioFiles(
        context: Context,
        jp: Vector<String>,
        onProgressBar: () -> Unit?,
        updateProgressBar: (Int) -> Unit?
    ): Boolean {
        val removeFileList = Vector<String>()
        for (i in fsAudioFileList) {
            if (i.contains("_")) {
                continue
            }
            val jpItem = i.replace(".wav", "")
            if (jp.contains(jpItem)) {
                continue
            }
            removeFileList.addElement(i)
            val jpWithSuffixFileList = getJpFilesNameVector(fsAudioFileList, jpItem)
            removeFileList += jpWithSuffixFileList
        }
        if (removeFileList.isEmpty()) {
            return true
        }
        val fNum = removeFileList.size
        var fCounter = 0
        updatedAudioList.clear()
        onProgressBar()
        updateProgressBar(0)
        for (i in removeFileList) {
            if (!FsClient.removeAudioFile(context, i)) {
                return false
            }
            fsAudioFileList.removeElement(i)
            fCounter++
            updateProgressBar((100.0F / fNum * fCounter).toInt())
        }
        return true
    }

// Получить вектор названий аудиофайлов, являющийся
//            вектором вариантов озвучки на 1 слово
    private fun getJpFilesNameVector(fileNames: Vector<String>, jp: String): Vector<String> {
        val rv = Vector<String>()
        for (i in fileNames) {
            if (i.contains(jp + "_")) {
                rv.addElement(i)
            }
        }
        return rv
    }

    fun isAudioExist(jp: String): Boolean {
        return fsAudioFileList.contains("$jp.wav")
    }

    fun playAudio(context: Context, jp: String) {
        // Выбрана ли опция "воспроизводить аудио"
        if (!SettingsClient(context).getAudioPlayingState()) {
            return
        }
        val mp = MediaPlayer()
        val variantNumber = getAudioVariantNumber(jp)
        if (variantNumber == 0) {
            return
        }
        val topRange = variantNumber - 1
        val i = (0..topRange).random()
        val path = if (i == 0) {
            context.filesDir.absolutePath + "/audio/jp/$jp.wav"
        } else {
            context.filesDir.absolutePath + "/audio/jp/" + jp + "_" + i.toString() + ".wav"
        }
        try {
            mp.setDataSource(path)
            mp.prepare()
            mp.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // Функция для получения кол-ва вариантов аудиозаписей на 1 слово
    private fun getAudioVariantNumber(jp: String): Int {
        if (!isAudioExist(jp)) {
            return 0
        }
        var num = 1
        while (true) {
            if (!fsAudioFileList.contains(jp + "_" + num.toString() + ".wav")) {
                return num // +1, first audio without number.
            }
            num++
        }
    }

    private fun isContainsAudio(filename: String): Boolean {
        return updatedAudioList.contains(filename)
    }

    private fun isUpdateAudioDateMatch(filename: String): Boolean {
        val calendar = Calendar.getInstance()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val formattedDate = formatter.format(calendar.time)
        return updatedAudioList.get(filename) == formattedDate
    }

    private fun setUpdatedAudio(filename: String) {
        val calendar = Calendar.getInstance()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val formattedDate = formatter.format(calendar.time)
        updatedAudioList.set(filename, formattedDate)
    }

    fun initUpdatedAudioList(fileData: String): Boolean {
        val lines = fileData.split('\n')
        for (line in lines) {
            if (line.isNotEmpty() && !parseUpdatedAudioListLine(line)) {
                return false
            }
        }
        return true
    }

    private fun parseUpdatedAudioListLine(line: String): Boolean {
        val o = UpdatedAudioList()
        val itemFields = line.split('\t')
        if (itemFields.size != ITEM_AU_LI_NUM) {
            return false
        }
        o.audiofilename = itemFields[ItemUpAuLiDefPos.audiofilename.value]
        o.date = itemFields[ItemUpAuLiDefPos.date.value]
        updatedAudioList.set(o.audiofilename, o.date)
        return true
    }

    private fun getAuListStr(aulist: MutableMap<String, String>): String {
        var str = String()
        for (i in aulist.entries) {
            str += i.key + '\t' + i.value + '\n'
        }
        return str
    }

    private enum class ItemUpAuLiDefPos(val value: Int) {
        audiofilename(0),
        date(1),
    }

    private const val ITEM_AU_LI_NUM = 2

    private val updatedAudioList: MutableMap<String, String> = mutableMapOf()
}